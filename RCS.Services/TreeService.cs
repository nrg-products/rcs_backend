﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RCS.Infrastructure.Models;
using RCS.Infrastructure.Services;

namespace RCS.Services
{
    public class TreeService : ITreeService
    {
        private readonly IRequirementService _requirementService;
        private readonly IFolderService _folderService;

        public TreeService(IFolderService folderService, IRequirementService requirementService)
        {
            _folderService = folderService;
            _requirementService = requirementService;
        }

        public async Task<List<TreeDto>> GetTree(int projectId)
        {
            var folders = await _folderService.GetAllFolders(projectId);
            var requirements = await _requirementService.GetAllRequirements(projectId);
            var tree = new List<TreeDto>();

            var n = folders.Count;
            for (var i = 0; i < n; i++)
            {
                tree.Add(new TreeDto(
                    folders[i].Id, 
                    folders[i].Num,
                    folders[i].Name, 
                    folders[i].ParentId,
                    folders[i].Created,
                    folders[i].Modified,
                    folders[i].Type,
                    folders[i].Collapsed
                    ));
            }
            n = requirements.Count;
            for (var i = 0; i < n; i++)
            {
                tree.Add(new TreeDto(
                    requirements[i].Id,
                    requirements[i].Num,
                    requirements[i].Name,
                    requirements[i].ParentId,
                    requirements[i].Created,
                    requirements[i].Modified,
                    requirements[i].Type,
                    requirements[i].VersionString,
                    requirements[i].Collapsed,
                    requirements[i].FixedVersionStatus
                    ));
            }
            var treeDfs = new TreeDfsTransfomer(tree);
            tree = treeDfs.Run();
            return tree;
        }
    }
}

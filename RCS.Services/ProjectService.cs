﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RCS.Infrastructure;
using RCS.Infrastructure.Models;
using RCS.Infrastructure.Services;

namespace RCS.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IProjectRepository _projectRepository;
        private readonly ICounterRepository _counterRepository;
        private readonly IRoleRepository _roleRepository;
        private readonly IMemberRepository _memberRepository;

        public ProjectService(IProjectRepository projectRepository, ICounterRepository counterRepository,
            IRoleRepository roleRepository, IMemberRepository memberRepository)
        {
            _projectRepository = projectRepository;
            _counterRepository = counterRepository;
            _roleRepository = roleRepository;
            _memberRepository = memberRepository;
        }

        public async Task<List<ProjectDto>> GetAllProjects()
        {
            return await _projectRepository.GetAllProjectsAsync();
        }

        public async Task<List<ProjectDto>> GetProjectsByEmail(string email)
        {
            return await _projectRepository.GetProjectsByEmailAsync(email);
        }

        public async Task<ProjectDto> GetProject(int projectNum)
        {
            return await _projectRepository.GetProjectAsync(projectNum);
        }

        public async Task<ProjectDto> AddProject(ProjectDto item)
        {
            var counter = await _counterRepository.AddCounter("projects");
            var project = await _projectRepository.AddProjectAsync(item, counter.Num);
            var defaultRoles = new List<RoleDto>
            {
                new RoleDto
                {
                    Name = "Viewer",
                    ProjectId = project.Id,
                    Approval = "Not required",
                    ProjectAccess = new ProjectAccess
                    {
                        CreateEditFolder = false,
                        CreateRequirement = false,
                        EditRequirement = false,
                        FixRequirement = false,
                        Invite = false
                    }
                },
                new RoleDto
                {
                    Name = "Collaborator",
                    ProjectId = project.Id,
                    Approval = "Not required",
                    ProjectAccess = new ProjectAccess
                    {
                        CreateEditFolder = true,
                        CreateRequirement = true,
                        EditRequirement = true,
                        FixRequirement = true,
                        Invite = false
                    }
                },
                new RoleDto
                {
                    Name = "Administrator",
                    ProjectId = project.Id,
                    Approval = "Not required",
                    ProjectAccess = new ProjectAccess
                    {
                        CreateEditFolder = true,
                        CreateRequirement = true,
                        EditRequirement = true,
                        FixRequirement = true,
                        Invite = true
                    }
                }
            };
            foreach (var role in defaultRoles)
            {
                await _roleRepository.AddRoleAsync(role);
            }
            return project;
        }

        public async Task<bool> RemoveProject(int projectNum)
        {
            return await _projectRepository.RemoveProjectAsync(projectNum);
        }

        public async Task<bool> UpdateProject(int projectNum, ProjectDto item)
        {
            return await _projectRepository.UpdateProjectAsync(projectNum, item);
        }

        public async Task<bool> AddMember(int projectNum, MemberDto member)
        {
            var project = await _projectRepository.GetProjectAsync(projectNum);
            var memberResult = await _memberRepository.AddMemberAsync(member, "owner", "owner", project.Id);
            return await _projectRepository.AddMemberAsync(projectNum, new ProjectMember {Email = memberResult.Email, Id = memberResult.Id, Role = memberResult.Role});
        }

        public async Task<bool> RemoveProjectMember(string memberId)
        {
            return await _projectRepository.RemoveProjectMember(memberId);
        }

        public async Task<List<RoleDto>> GetRoles(int projectNum)
        {
            var project = await _projectRepository.GetProjectAsync(projectNum);
            return await _roleRepository.GetRolesAsync(project.Id);
        }

        public async Task<bool> UpdateProjectMembers(int projectNum, ProjectDto project)
        {
            return await _projectRepository.UpdateProjectMembersAsync(projectNum, project);
        }
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RCS.Infrastructure;
using RCS.Infrastructure.Models;
using RCS.Infrastructure.Services;

namespace RCS.Services
{
    public class FolderService : IFolderService
    {
        private readonly IFolderRepository _folderRepository;
        private readonly IProjectRepository _projectRepository;
        private readonly ICounterRepository _counterRepository;

        public FolderService(IFolderRepository folderRepository, IProjectRepository projectRepository, ICounterRepository counterRepository)
        {
            _folderRepository = folderRepository;
            _projectRepository = projectRepository;
            _counterRepository = counterRepository;
        }

        public async Task<List<FolderDto>> GetAllFolders(int projectNum)
        {
            var project = await _projectRepository.GetProjectAsync(projectNum);
            return await _folderRepository.GetAllFoldersAsync(project.Id);
        }

        public async Task<FolderDto> GetFolder(int projectNum, int folderNum)
        {
            var project = await _projectRepository.GetProjectAsync(projectNum);
            return await _folderRepository.GetFolderAsync(project.Id, folderNum);
        }

        public async Task<bool> AddFolder(int projectNum, FolderDto item)
        {
            var project = await _projectRepository.GetProjectAsync(projectNum);
            var counter = await _counterRepository.AddCounter(project.Id + " requirements");
            return await _folderRepository.AddFolderAsync(project.Id, counter.Num, item);
        }

        public async Task<bool> UpdateFolder(int projectNum, int folderNum, FolderDto item)
        {
            var project = await _projectRepository.GetProjectAsync(projectNum);
            return await _folderRepository.UpdateFolderAsync(project.Id, folderNum, item);
        }
    }
}

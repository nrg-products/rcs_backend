﻿using RCS.Infrastructure.Models;
using System.Collections.Generic;
using System.Linq;

namespace RCS.Services
{
    public class TreeDfsTransfomer
    {
        private List<TreeDto> Tree;
        private bool[] Used;
        
        public TreeDfsTransfomer(List<TreeDto> tree)
        {
            this.Tree = tree;
            Used = new bool[tree.Count];
        }

        public List<TreeDto> Run()
        {
            var adjList = new GraphAdjList(Tree);
            for (var i = 0; i < adjList.Tree.Count; i++)
            {
                if (Used[i]) continue;
                Dfs(adjList, i);
            }

            var count = Tree.Count;
            for (var i = 0; i < count; i++)
            {
                if (i >= Tree.Count) continue;
                if (Tree[i].ParentId == null) continue;
                Tree.RemoveAt(i);
                i--;
            }

            return Tree;
        }
        public void Dfs(GraphAdjList adjList, int v)
        {
            Used[v] = true;
            var newAdjList = new GraphAdjList(adjList.GetAdj(v));
            for (var i = 0; i < newAdjList.VertexCount; i++)
            {
                if (!Used[adjList.Tree.FindIndex(a => a.Id == adjList.Tree[v].Id)])
                {
                    Dfs(newAdjList, i);
                }
            }
            Tree[v].SubItems = new List<TreeDto>();
            foreach (var item in newAdjList.Tree)
            {
                Tree[v].SubItems.Add(item);
            }
        }

        public class GraphAdjList
        {
            public List<TreeDto> Tree { get; set; }
            public int VertexCount { get; set; }
            public GraphAdjList(List<TreeDto> tree)
            {
                Tree = tree;
                VertexCount = tree.Count;
            }
            public List<TreeDto> GetAdj(int v)
            {
                return Tree.Where(t => t.ParentId == Tree[v].Id).ToList();
            }
        }
    }
}

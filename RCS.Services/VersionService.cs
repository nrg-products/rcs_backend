﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RCS.Infrastructure;
using RCS.Infrastructure.Models;
using RCS.Infrastructure.Services;

namespace RCS.Services
{
    public class VersionService : IVersionService
    {
        private readonly IVersionRepository _versionRepository;
        private readonly IProjectRepository _projectRepository;
        private readonly IRequirementRepository _requirementRepository;

        public VersionService(IVersionRepository versionRepository, IProjectRepository projectRepository, IRequirementRepository requirementRepository)
        {
            _versionRepository = versionRepository;
            _projectRepository = projectRepository;
            _requirementRepository = requirementRepository;
        }
        public async Task<List<VersionsDto>> GetVersions(int projectNum, int requirementNum)
        {
            var project = await _projectRepository.GetProjectAsync(projectNum);
            var requirement = await _requirementRepository.GetRequirementAsync(project.Id, requirementNum);
            return await _versionRepository.GetAllVersionsAsync(requirement.Id);
        }

        public async Task<List<HistoryDto>> GetHistory(string requirementId)
        {
            return await _versionRepository.GetAllHistoryAsync(requirementId);
        }

        public async Task<bool> CreateComment(HistoryDto model)
        {
            return await _versionRepository.CreateCommentAsync(model);
        }

        public async Task<VersionsDto> GetVersion(string requirementId)
        {
            return await _versionRepository.GetVersionAsync(requirementId);
        }

        public async Task<bool> UpdateHistoryAndVersion(HistoryDto model)
        {
            await _versionRepository.UpdateVersionAsync(model);
            return await _versionRepository.UpdateHistoryAsync(model);
        }
    }
}

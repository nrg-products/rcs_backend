﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Amazon;
using Amazon.IdentityManagement.Model;
using RCS.Infrastructure.Services;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using RCS.Infrastructure;
using RCS.Infrastructure.Exeptions;
using RCS.Infrastructure.Models;

namespace RCS.Services
{
    public class MemberService : IMemberService
    {
        private readonly IMemberRepository _memberRepository;
        private readonly IProjectRepository _projectRepository;

        public MemberService(IMemberRepository memberRepository, IProjectRepository projectRepository)
        {
            _memberRepository = memberRepository;
            _projectRepository = projectRepository;
        }

        public string SendEmail(string email, ProjectDto project, string invitationKey)
        {
            const string accessKeyId = "AKIAIE2BF4EB6QNDG5OA";
            const string secretAccessKey = "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";
            var messageId = "";
            var memberLink = "http://rcs.nrg-soft.com/" + project.Num+"/invitation/" + invitationKey;

            var amConfig = new AmazonSimpleEmailServiceConfig {RegionEndpoint = RegionEndpoint.EUWest1};
            var amzClient = new AmazonSimpleEmailServiceClient(accessKeyId, secretAccessKey, amConfig);

            var dest = new Destination
            {
                ToAddresses = new List<string> {email}
            };
            var title =
                new Content("You have been invited to join project \"" + project.Name + "\" on Requirements Control Service");
            var body = new Body {Html = new Content("Join by clicking here: <a href =\"" + memberLink + "\">link</a>")};
            var message = new Message(title, body);

            var ser = new SendEmailRequest("Requirements Control Service <system@nrg-soft.com>", dest, message);

            try
            {
                var seResponse = amzClient.SendEmail(ser);
                messageId = seResponse.MessageId;
            }
            catch (AmazonSimpleEmailServiceException ex)
            {
                throw new AwsEmailExeption(ex.Message);
            }

            return messageId;
        }

        public async Task<List<MemberDto>> GetAllMembers(int projectNum)
        {
            var project = await _projectRepository.GetProjectAsync(projectNum);
            return await _memberRepository.GetAllMembersAsync(project.Id);
        }

        public async Task<MemberDto> SendMember(int projectNum, MemberDto item)
        {
            var project = await _projectRepository.GetProjectAsync(projectNum);
            var invitationKey = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
            invitationKey = invitationKey.Replace("=", "").Replace("+", "").Replace("/", "").Replace("\\", "");
            var messageId = SendEmail(item.Email, project, invitationKey);
            return await _memberRepository.AddMemberAsync(item, messageId, invitationKey, project.Id);
        }

        public async Task<bool> DeleteMember(string memberId)
        {
            await _projectRepository.RemoveProjectMember(memberId);
            return await _memberRepository.DeleteMemberAsync(memberId);
        }

        public async Task<bool> UpdateMemberRole(string memberId, RoleDto role)
        {
            return await _memberRepository.UpdateMemberRoleAsync(memberId, role);
        }

        public async Task<bool> AcceptMember(int projectNum, string invitationKey)
        {
            await _memberRepository.UpdateMemberStatusAsync(invitationKey);
            var member = await _memberRepository.GetMemberAsync(invitationKey);
            var projectMember = new ProjectMember
            {
                Email = member.Email,
                Id = member.Id,
                Role = member.Role
            };
            return await _projectRepository.AddMemberAsync(projectNum, projectMember);
        }
        public async Task<MemberDto> GetMember(string invitationKey)
        {
           return await _memberRepository.GetMemberAsync(invitationKey);
        }
    }
}

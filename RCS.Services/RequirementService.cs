﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RCS.Infrastructure;
using RCS.Infrastructure.Models;
using RCS.Infrastructure.Services;

namespace RCS.Services
{
    public class RequirementService : IRequirementService
    {
        private readonly IRequirementRepository _requirementRepository;
        private readonly IProjectRepository _projectRepository;
        private readonly ICounterRepository _counterRepository;
        private readonly IVersionRepository _versionRepository;

        public RequirementService(IRequirementRepository requirementRepository, IProjectRepository projectRepository, ICounterRepository counterRepository, IVersionRepository versionRepository)
        {
            _requirementRepository = requirementRepository;
            _projectRepository = projectRepository;
            _counterRepository = counterRepository;
            _versionRepository = versionRepository;
        }


        public async Task<List<RequirementDto>> GetAllRequirements(int projectNum)
        {
            var project = await _projectRepository.GetProjectAsync(projectNum);
            return await _requirementRepository.GetAllRequirementsAsync(project.Id);
        }

        public async Task<RequirementDto> GetRequirement(int projectNum, int requirementNum)
        {
            var  project = await _projectRepository.GetProjectAsync(projectNum);
            return await _requirementRepository.GetRequirementAsync(project.Id, requirementNum);
        }

        public async Task<bool> AddRequirementAsync(int projectNum, RequirementDto item)
        {
            var project = await _projectRepository.GetProjectAsync(projectNum);
            var counter = await _counterRepository.AddCounter(project.Id + " requirements");
            var requirementId = await _requirementRepository.AddRequirementAsync(project.Id, counter.Num, item);
            item.Id = requirementId;
            await _versionRepository.CreateVersionAsync(item);
            return true;
        }

        public async Task<bool> UpdateRequirement(int projectNum, RequirementDto item)
        {
            var project = await _projectRepository.GetProjectAsync(projectNum);
            await _versionRepository.CreateHistoryAsync(item);
            return await _requirementRepository.UpdateRequirement(project.Id, item.Id, item);
        }

        public async Task<string> UpdateRequirementMajor(int projectNum, RequirementDto item)
        {
            var project = await _projectRepository.GetProjectAsync(projectNum);
            var version = await _versionRepository.GetVersionAsync(item.Id);
            var newVersion = await _versionRepository.AddVersionAsync(item, "major", version.MajorVersion, version.MinorVersion);
            await _requirementRepository.UpdateRequirementMajor(project.Id, item.Id, item, newVersion.VersionString);
            return newVersion.VersionString;
        }

        public async Task<string> UpdateRequirementMinor(int projectNum, RequirementDto item)
        {
            var project = await _projectRepository.GetProjectAsync(projectNum);
            var version = await _versionRepository.GetVersionAsync(item.Id);
            var newVersion = await _versionRepository.AddVersionAsync(item, "minor", version.MajorVersion, version.MinorVersion);
            await _requirementRepository.UpdateRequirementMinor(project.Id, item.Id, item, newVersion.VersionString);
            return newVersion.VersionString;
        }

        public async Task<bool> UpdateRequirementParent(int projectNum,  RequirementDto item)
        {
            var project = await _projectRepository.GetProjectAsync(projectNum);
            return await _requirementRepository.UpdateRequirementParent(project.Id, item.Id, item);
        }
    }
}

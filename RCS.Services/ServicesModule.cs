﻿using Autofac;
using RCS.Infrastructure.Services;

namespace RCS.Services
{
    public class ServicesModule:Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<TreeService>().As<ITreeService>();
            builder.RegisterType<MemberService>().As<IMemberService>();
            builder.RegisterType<RequirementService>().As<IRequirementService>();
            builder.RegisterType<FolderService>().As<IFolderService>();
            builder.RegisterType<ProjectService>().As<IProjectService>();
            builder.RegisterType<VersionService>().As<IVersionService>();
        }
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RCS.Infrastructure.Models;

namespace RCS.Infrastructure
{
    public interface IProjectRepository
    {
        Task<List<ProjectDto>> GetAllProjectsAsync();

        Task<List<ProjectDto>> GetProjectsByEmailAsync(string email);

        Task<ProjectDto> GetProjectAsync(int projectNum);

        Task<ProjectDto> AddProjectAsync(ProjectDto item, int counterNum);

        Task<bool> RemoveProjectAsync(int projectNum);

        Task<bool> UpdateProjectAsync(int projectNum, ProjectDto item);

        Task<bool> AddMemberAsync(int projectNum, ProjectMember member);

        Task<bool> RemoveProjectMember(string memberId);

        Task<bool> UpdateProjectMembersAsync(int projectNum, ProjectDto project);
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RCS.Infrastructure.Models;

namespace RCS.Infrastructure
{
    public interface IMemberRepository
    {
        Task<List<MemberDto>> GetAllMembersAsync(string projectId);

        Task<MemberDto> AddMemberAsync(MemberDto item, string messageId, string invitationKey, string projectId);

        Task<bool> DeleteMemberAsync(string memberId);

        Task<bool> UpdateMemberStatusAsync(string memberId);

        Task<MemberDto> GetMemberAsync(string invitationKey); 

        Task<bool> UpdateMemberRoleAsync(string memberId, RoleDto role);
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RCS.Infrastructure.Models;

namespace RCS.Infrastructure
{
    public interface IVersionRepository
    {
        Task<List<VersionsDto>> GetAllVersionsAsync(string requirementId);

        Task<List<HistoryDto>> GetAllHistoryAsync(string requirementId);

        Task<VersionsDto> GetVersionAsync(string requirementId);

        Task<VersionsDto> AddVersionAsync(RequirementDto item, string type, int major, int minor);

        Task<VersionsDto> CreateVersionAsync(RequirementDto item);

        Task<bool> CreateHistoryAsync(RequirementDto item);

        Task<bool> CreateCommentAsync(HistoryDto item);

        Task<bool> UpdateHistoryAsync(HistoryDto item);

        Task<bool> UpdateVersionAsync(HistoryDto item);
    }
}

﻿namespace RCS.Infrastructure.Models
{
    public class CounterDto
    {
        public string Id { get; set; }

        public string Key { get; set; }

        public int Num { get; set; }
    }
}

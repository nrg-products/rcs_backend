﻿using System;
using System.Collections.Generic;

namespace RCS.Infrastructure.Models
{
    public class TreeDto
    {
        public TreeDto(string id, int num, string name, string parentId, DateTime created, DateTime modified,
            string type, bool collapsed)
        {
            Id = id;
            Num = num;
            Name = name;
            ParentId = parentId;
            Created = created;
            Modified = modified;
            Type = type;
            Collapsed = collapsed;
        }

        public TreeDto(string id, int num, string name, string parentId,
            DateTime created, DateTime modified, string type, string versionString, bool collapsed, string status)
        {
            Id = id;
            Num = num;
            Name = name;
            ParentId = parentId;
            Created = created;
            Modified = modified;
            Type = type;
            VersionString = versionString;
            Collapsed = collapsed;
            Status = status;
        }

        public string Id { get; set; }

        public int Num { get; set; }

        public string Name { get; set; }

        public string ParentId { get; set; }

        public List<TreeDto> SubItems { get; set; }

        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }

        public string Type { get; set; }

        public string VersionString { get; set; }

        public string Status { get; set; }

        public bool Collapsed { get; set; }
    }
}

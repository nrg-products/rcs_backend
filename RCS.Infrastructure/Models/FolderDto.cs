﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RCS.Infrastructure.Models
{
    public class FolderDto
    {
        public string Id { get; set; }

        public int Num { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        public string ParentId { get; set; }

        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }

        public string Type { get; set; }

        public bool Collapsed { get; set; }
    }
}
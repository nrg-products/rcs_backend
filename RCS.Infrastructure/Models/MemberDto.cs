﻿using System.ComponentModel.DataAnnotations;

namespace RCS.Infrastructure.Models
{
    public class MemberDto
    {
        public string ProjectId { get; set; }

        public string Id { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public RoleDto Role { get; set; }

        public string State { get; set; }

        public string MessageId { get; set; }
    }
}

﻿using System;

namespace RCS.Infrastructure.Models
{
    public class HistoryDto
    {
        public string Id { get; set; }

        public ObjectCopyDto ObjectCopy { get; set; }

        public string StringTemplate { get; set; }

        public string Comment { get; set; }

        public string VersionId { get; set; }

        public string UpdatedField { get; set; }

        public DateTime Created { get; set; }

        public UserDto CreatedBy { get; set; }
    }
}

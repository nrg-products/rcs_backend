﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RCS.Infrastructure.Models
{
    public class RequirementDto
    {
        public string Id { get; set; }

        public int Num { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        public string Source { get; set; }

        public string IssueLink { get; set; }

        public string Priority { get; set; }

        public string Status { get; set; }

        public DateTime DueDate { get; set; }

        public string ParentId { get; set; }

        public string Type { get; set; }

        public DateTime Created { get; set; }

        public UserDto CreatedBy { get; set; }

        public DateTime Modified { get; set; }

        public UserDto ModifiedBy { get; set; }

        public string UpdatedField { get; set; }

        public string VersionString { get; set; }

        public string FixedVersionStatus { get; set; }

        public bool Collapsed { get; set; }
    }
}
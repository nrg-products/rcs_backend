﻿namespace RCS.Infrastructure.Models
{
    public class RoleDto
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Approval { get; set; }

        public string ProjectId { get; set; }

        public ProjectAccess ProjectAccess { get; set; }
    }
    public class ProjectAccess
    {
        public bool CreateRequirement { get; set; }

        public bool EditRequirement { get; set; }

        public bool CreateEditFolder { get; set; }

        public bool Invite { get; set; }

        public bool FixRequirement { get; set; }
    }
}

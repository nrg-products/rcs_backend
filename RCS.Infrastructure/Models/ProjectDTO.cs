﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RCS.Infrastructure.Models
{
    public class ProjectDto
    {
        public string Id { get; set; }

        public int Num { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Key { get; set; }

        public List<ProjectMember> Members { get; set; }
    }

    public class ProjectMember
    {
        public string Email { get; set; }

        public RoleDto Role { get; set; }

        public string Id { get; set; }
    }
}
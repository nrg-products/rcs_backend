﻿using System;

namespace RCS.Infrastructure.Models
{
    public class ObjectCopyDto
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string Source { get; set; }

        public DateTime DueDate { get; set; }

        public string FixedVersionStatus { get; set; }

        public string IssueLink { get; set; }

        public string VersionString { get; set; }
    }

    public class VersionsDto
    {
        public string Id { get; set; }

        public int MinorVersion { get; set; }

        public int MajorVersion { get; set; }

        public string VersionString { get; set; }

        public ObjectCopyDto ObjectCopy { get; set; }
        
        public DateTime Created { get; set; }

        public UserDto CreatedBy { get; set; }
    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RCS.Infrastructure.Models;

namespace RCS.Infrastructure.Services
{
    public interface ITreeService
    {
        Task<List<TreeDto>> GetTree(int projectId);
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RCS.Infrastructure.Models;

namespace RCS.Infrastructure.Services
{
    public interface  IRequirementService
    {
        Task<List<RequirementDto>> GetAllRequirements(int projectNum);

        Task<RequirementDto> GetRequirement(int projectNum, int requirementNum);

        Task<bool> AddRequirementAsync(int projectNum, RequirementDto item);

        Task<bool> UpdateRequirement(int projectNum, RequirementDto item);

        Task<string> UpdateRequirementMajor(int projectNum, RequirementDto item);

        Task<string> UpdateRequirementMinor(int projectNum, RequirementDto item);

        Task<bool> UpdateRequirementParent(int projectNum, RequirementDto item);
    }
}

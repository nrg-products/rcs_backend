﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RCS.Infrastructure.Models;

namespace RCS.Infrastructure.Services
{
    public interface IVersionService
    {
        Task<List<VersionsDto>> GetVersions(int projectNum , int requirementNum);

        Task<List<HistoryDto>> GetHistory(string requirementId);

        Task<bool> CreateComment(HistoryDto model);

        Task<VersionsDto> GetVersion(string requirementId);

        Task<bool> UpdateHistoryAndVersion(HistoryDto model);
    }
}

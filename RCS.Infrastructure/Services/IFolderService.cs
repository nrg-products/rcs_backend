﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RCS.Infrastructure.Models;

namespace RCS.Infrastructure.Services
{
    public interface IFolderService
    {
        Task<List<FolderDto>> GetAllFolders(int projectNum);

        Task<FolderDto> GetFolder(int projectNum, int folderNum);

        Task<bool> AddFolder(int projectNum, FolderDto item);

        Task<bool> UpdateFolder(int projectNum, int folderNum, FolderDto item);
    }
}

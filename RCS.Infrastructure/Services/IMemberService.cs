﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RCS.Infrastructure.Models;

namespace RCS.Infrastructure.Services
{
    public interface IMemberService
    {
        Task<List<MemberDto>> GetAllMembers(int projectNum);

        Task<MemberDto> SendMember(int peojectNum, MemberDto item);

        Task<bool> DeleteMember(string memberId);

        Task<bool> UpdateMemberRole(string memberId, RoleDto role);

        Task<bool> AcceptMember(int projectNum, string invitationKey);

        Task<MemberDto> GetMember(string invitationKey);
    }
}

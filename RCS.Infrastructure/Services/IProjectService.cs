﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RCS.Infrastructure.Models;

namespace RCS.Infrastructure.Services
{
    public interface IProjectService
    {
        Task<List<ProjectDto>> GetAllProjects();

        Task<List<ProjectDto>> GetProjectsByEmail(string email);

        Task<ProjectDto> GetProject(int projectNum);

        Task<ProjectDto> AddProject(ProjectDto item);

        Task<bool> RemoveProject(int projectNum);

        Task<bool> UpdateProject(int projectNum, ProjectDto item);

        Task<bool> AddMember(int projectNum, MemberDto member);

        Task<bool> RemoveProjectMember(string memberId);

        Task<List<RoleDto>> GetRoles(int projectNum);

        Task<bool> UpdateProjectMembers(int projectNum, ProjectDto project);
    }
}

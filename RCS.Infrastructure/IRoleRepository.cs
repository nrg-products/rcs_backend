﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RCS.Infrastructure.Models;

namespace RCS.Infrastructure
{
    public interface IRoleRepository
    {
        Task<List<RoleDto>> GetRolesAsync(string projectId);

        Task<RoleDto> AddRoleAsync(RoleDto item);
    }
}

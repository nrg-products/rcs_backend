﻿using System.Threading.Tasks;
using RCS.Infrastructure.Models;

namespace RCS.Infrastructure
{
    public interface ICounterRepository
    {
        Task<CounterDto> AddCounter(string key);
    }
}

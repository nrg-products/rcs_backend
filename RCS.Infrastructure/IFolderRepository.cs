﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RCS.Infrastructure.Models;

namespace RCS.Infrastructure
{
    public interface IFolderRepository
    {
        Task<List<FolderDto>> GetAllFoldersAsync(string projectId);

        Task<FolderDto> GetFolderAsync(string projectId, int folderNum);

        Task<bool> AddFolderAsync(string projectId, int counterNum, FolderDto item);

        Task<bool> UpdateFolderAsync(string projectId, int folderNum, FolderDto item);
    }
}

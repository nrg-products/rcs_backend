﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RCS.Infrastructure.Models;

namespace RCS.Infrastructure
{
    public interface IRequirementRepository
    {
        Task<List<RequirementDto>> GetAllRequirementsAsync(string projectId);

        Task<RequirementDto> GetRequirementAsync(string projectId, int requirementNum);

        Task<string> AddRequirementAsync(string projectId, int counterNum, RequirementDto item);

        Task<bool> UpdateRequirement(string projectId, string requirementId, RequirementDto item);

        Task<bool> UpdateRequirementMajor(string projectId, string requirementId, RequirementDto item, string versionString);

        Task<bool> UpdateRequirementMinor(string projectId, string requirementId, RequirementDto item, string versionString);

        Task<bool> UpdateRequirementParent(string projectId, string requirementId, RequirementDto item);
    }
}

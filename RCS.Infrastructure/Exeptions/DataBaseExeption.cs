﻿using System;

namespace RCS.Infrastructure.Exeptions
{
    public class DataBaseExeption: Exception
    {
        public DataBaseExeption(string message)
        {
            ErrorMessage = message;
        }
        public string ErrorMessage { get; private set; }
    }
}

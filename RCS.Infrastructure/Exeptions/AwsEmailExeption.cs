﻿using System;

namespace RCS.Infrastructure.Exeptions
{
    public class AwsEmailExeption : Exception
    {
        public AwsEmailExeption(string message)
        {
            ErrorMessage = message;
        }
        public string ErrorMessage { get; private set; }
    }
}

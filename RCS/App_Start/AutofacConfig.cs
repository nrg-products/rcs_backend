﻿using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using RCS.Repositories;
using RCS.Services;

namespace RCS.App_Start
{
    public static class AutofacConfig
    {
        public static IContainer Register(HttpConfiguration config)
        {
            var builder = new ContainerBuilder();

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterWebApiFilterProvider(config);
            builder.RegisterModule(new RepositoriesModule());
            builder.RegisterModule(new ServicesModule());

            var iocContainer = builder.Build();
            var resolver = new AutofacWebApiDependencyResolver(iocContainer);
            config.DependencyResolver = resolver;
            return iocContainer;
        }
    }
}
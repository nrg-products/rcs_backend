﻿using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;
using Newtonsoft.Json.Serialization;
using RCS.Filters;
using RCS.Infrastructure.Exeptions;

namespace RCS
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Filters.Add(new AuthorizeAttribute());
            config.Filters.Add(new ModelValidationFilterAttribute());
            config.Filters.Add(new NotFoundExceptionFilterAttribute());
            config.Filters.Add(new DatabaseExceptionFilterAttribute());
        }
    }
}

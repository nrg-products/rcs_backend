﻿using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;
using RCS.Infrastructure.Exeptions;

namespace RCS.Filters
{
    public class NotFoundExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            if (!(context.Exception is EntityNotFoundException)) return;
            var exception = (EntityNotFoundException) context.Exception;
            context.Response = new HttpResponseMessage(HttpStatusCode.NotFound);
        }
    }
}
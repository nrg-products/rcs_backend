﻿using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http.Filters;
using RCS.Infrastructure.Exeptions;

namespace RCS.Filters
{
    public class DatabaseExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            if (!(context.Exception is DataBaseExeption)) return;
            var exception = (DataBaseExeption) context.Exception;
            context.Response = new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
                Content = new ObjectContent<dynamic>(
                    new
                    {
                        message = exception.ErrorMessage,
                    },
                    new JsonMediaTypeFormatter(), "application/json")
            }; 
        }
    }
}
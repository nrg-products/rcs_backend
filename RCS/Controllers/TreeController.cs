﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using RCS.Infrastructure.Services;

namespace RCS.Controllers
{
    public class TreeController : ApiController
    {
        private readonly ITreeService _treeService;

        public TreeController(ITreeService treeService)
        {
            _treeService = treeService;
        }

        //get tree
        [Route("projects/{projectNum:int}/tree")]
        public async Task<IHttpActionResult> GetTree(int projectNum)
        {
            var result = await _treeService.GetTree(projectNum);
            var ordered = result.OrderBy(x => x.Id);
            return Ok(ordered);
        }
  
    }
}

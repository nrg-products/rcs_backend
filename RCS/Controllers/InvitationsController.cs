﻿using System.Threading.Tasks;
using System.Web.Http;
using RCS.Infrastructure.Models;
using RCS.Infrastructure.Services;

namespace RCS.Controllers
{
    public class MembersController : ApiController
    {
        private readonly IMemberService _memberService;

        public MembersController(IMemberService memberService)
        {
            _memberService = memberService;
        }
        //get members
        [Route("projects/{projectNum:int}/members")]
        public async Task<IHttpActionResult> GetMembers(int projectNum)
        {
            var result = await _memberService.GetAllMembers(projectNum);
            return Ok(result.ToArray());
        }

        //get member
        [Route("members/{invitationKey}")]
        public async Task<IHttpActionResult> GetMember(string invitationKey)
        {
            var result = await _memberService.GetMember(invitationKey);
            return Ok(result);
        }

        //edit member role
        [HttpPut]
        [Route("projects/{projectNum:int}/members/{memberId}")]
        public async Task<IHttpActionResult> UpdateMemberRole(int projectNum, string memberId, MemberDto model)
        {
            var result = await _memberService.UpdateMemberRole(memberId, model.Role);
            return Ok(result);
        }

        //delete member
        [HttpDelete]
        [Route("projects/{projectNum:int}/members/{memberId}")]
        public async Task<IHttpActionResult> DeleteMember(int projectNum, string memberId)
        {
            var result = await _memberService.DeleteMember(memberId);
            return Ok(result);
        }

        //send invitation
        [HttpPost]
        [Route("projects/{projectNum:int}/members")]
        public async Task<IHttpActionResult> SendMember(int projectNum, MemberDto model)
        {
            var result = await _memberService.SendMember(projectNum, model);
            return Ok(result);
        }

        //accept member
        [HttpGet]
        [Route("projects/{projectNum:int}/members/{invitationKey}")]
        public async Task<IHttpActionResult> AcceptMember(int projectNum, string invitationKey)
        {
            var result = await _memberService.AcceptMember(projectNum, invitationKey);
            return Ok(result);
        }
    }
}
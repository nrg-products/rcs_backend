﻿using System.Linq;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Tracing;

namespace RCS.Controllers
{
    public class TestController : ApiController
    {
        [Route("test")]
        public IHttpActionResult Get()
        {
			//testt
            return Ok("tested");
        }

        [Route("protected")]
        [HttpGet]
        public IHttpActionResult TestAuth()
        {
            var identity = User.Identity as ClaimsIdentity;
            if (identity == null) return Unauthorized();

            return Ok(identity.Claims.Select(c => new
            {
                c.Type,
                c.Value
            }));
        }
    }
}

﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using RCS.Infrastructure.Models;
using RCS.Infrastructure.Services;

namespace RCS.Controllers
{
    public class ProjectsController: ApiController
    {
        private readonly IProjectService _projectService;

        public ProjectsController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        //create project
        [HttpPost]
        [Route("projects")]
        public async Task<IHttpActionResult> CreateProject(ProjectDto model)
        {
            var result = await _projectService.AddProject(model);
            return Ok(result);
        }
        //get projects
        [Route("projects")]
        public async Task<IHttpActionResult> GetProjects()
        {
            var result = await _projectService.GetAllProjects();
            return Ok(result.ToArray());
        }

        //get projects by email
        [Route("projects/email/")]
        public async Task<IHttpActionResult> GetProjectsByEmail(string email ="")
        {
            var result = await _projectService.GetProjectsByEmail(email);
            return Ok(result.ToArray());
        }

        //add project member
        [HttpPut]
        [Route("projects/{projectNum:int}/member")]
        public async Task<IHttpActionResult>AddMember(int projectNum, MemberDto model)
        {
            var result = await _projectService.AddMember(projectNum, model);
            return Ok(result);
        }

        //edit project member
        [HttpPut]
        [Route("projects/{projectNum:int}/member/role")]
        public async Task<IHttpActionResult> EditProjectMembers(int projectNum, ProjectDto model)
        {
            var result = await _projectService.UpdateProjectMembers(projectNum, model);
            return Ok(result);
        }

        //get project roles
        [Route("projects/{projectNum:int}/roles")]
        public async Task<IHttpActionResult> GetRoles(int projectNum)
        {
            var result = await _projectService.GetRoles(projectNum);
            var ordered = result.OrderBy(x => x.Id);
            return Ok(ordered);
        }

        //get project
        [Route("projects/{projectNum:int}")]
        public async Task<IHttpActionResult> GetProject(int projectNum)
        {
            var result = await _projectService.GetProject(projectNum);
            return Ok(result);
        }

        //edit project
        [HttpPut]
        [Route("projects/{projectNum:int}")]
        public async Task<IHttpActionResult> EditProject(int projectNum, ProjectDto model)
        {
            var result = await _projectService.UpdateProject(projectNum, model);
            return Ok(result);
        }

        //delete project
        [HttpDelete]
        [Route("projects/{projectNum:int}")]
        public async Task<IHttpActionResult> DeleteProject(int projectNum)
        {
            var result = await _projectService.RemoveProject(projectNum);
            return Ok(result);
        }
    }
}
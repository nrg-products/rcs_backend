﻿using System.Threading.Tasks;
using System.Web.Http;
using RCS.Filters;
using RCS.Infrastructure.Models;
using RCS.Infrastructure.Services;

namespace RCS.Controllers
{
    [NotFoundExceptionFilter]
    [ModelValidationFilter]
    public class FoldersController : ApiController
    {
        private readonly IFolderService _folderService;

        public FoldersController(IFolderService folderService)
        {
            _folderService = folderService;
        }

        //create folder
        [HttpPost]
        [Route("projects/{projectNum:int}/folders")]
        public async Task<IHttpActionResult> CreateFolder(int projectNum, FolderDto model)
        {
            var result = await _folderService.AddFolder(projectNum, model);
            return Ok(result);
        }
        //get folders
        [Route("projects/{projectNum:int}/folders")]
        public async Task<IHttpActionResult> GetFolders(int projectNum)
        {
            var result = await _folderService.GetAllFolders(projectNum);
            return Ok(result.ToArray());
        }

        //get folder
        [Route("projects/{projectNum:int}/folders/{folderNum:int}")]
        public async Task<IHttpActionResult> GetProject(int projectNum, int folderNum)
        {
            var result = await _folderService.GetFolder(projectNum, folderNum);
            return Ok(result);
        }

        //edit folder
        [HttpPut]
        [Route("projects/{projectNum:int}/folders/{folderNum:int}")]
        public async Task<IHttpActionResult> EditFolder(int projectNum, int folderNum, FolderDto model)
        {
            var result = await _folderService.UpdateFolder(projectNum, folderNum, model);
            return Ok(result);
        }
    }
}
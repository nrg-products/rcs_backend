﻿using System.Threading.Tasks;
using System.Web.Http;
using RCS.Filters;
using RCS.Infrastructure.Models;
using RCS.Infrastructure.Services;

namespace RCS.Controllers
{
    [ModelValidationFilter]
    public class RequirementsController : ApiController
    {
        private readonly IRequirementService _requirementService;

        public RequirementsController(IRequirementService requirementService)
        {
            _requirementService = requirementService;
        }

        //create requirement
        [HttpPost]
        [Route("projects/{projectNum:int}/requirements")]
        public async Task<IHttpActionResult> CreateRequirement(int projectNum, RequirementDto model)
        {
            var result = await _requirementService.AddRequirementAsync(projectNum, model);
            return Ok(result);
        }
        //get requirements
        [Route("projects/{projectNum:int}/requirements")]
        public async Task<IHttpActionResult> GetRequirements(int projectNum)
        {
            var result = await _requirementService.GetAllRequirements(projectNum);
            return Ok(result.ToArray());
        }

        //get requirement
        [Route("projects/{projectNum:int}/requirements/{requirementNum:int}")]
        public async Task<IHttpActionResult> GetRequirement(int projectNum, int requirementNum)
        {
            var result = await _requirementService.GetRequirement(projectNum, requirementNum);
            return Ok(result);
        }

        //edit requirement
        [HttpPut]
        [Route("projects/{projectNum:int}/requirements/{requirementNum:int}")]
        public async Task<IHttpActionResult> EditRequirement(int projectNum, RequirementDto model)
        {
            var result = await _requirementService.UpdateRequirement(projectNum, model);
            return Ok(result);
        }

        //edit requirement minor
        [HttpPut]
        [Route("projects/{projectNum:int}/requirements/{requirementNum:int}/minor")]
        public async Task<IHttpActionResult> EditRequirementMinor(int projectNum, RequirementDto model)
        {
            var result = await _requirementService.UpdateRequirementMinor(projectNum, model);
            return Ok(result);
        }

        //edit requirement major
        [HttpPut]
        [Route("projects/{projectNum:int}/requirements/{requirementNum:int}/major")]
        public async Task<IHttpActionResult> EditRequirementMajor(int projectNum, RequirementDto model)
        {
            var result = await _requirementService.UpdateRequirementMajor(projectNum, model);
            return Ok(result);
        }

        //edit requirement major
        [HttpPut]
        [Route("projects/{projectNum:int}/requirements/{requirementNum:int}/editParent")]
        public async Task<IHttpActionResult> EditRequirementParent(int projectNum, RequirementDto model)
        {
            var result = await _requirementService.UpdateRequirementParent(projectNum, model);
            return Ok(result);
        }

    }
}
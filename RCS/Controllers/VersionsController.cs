﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using RCS.Infrastructure;
using RCS.Infrastructure.Models;
using RCS.Infrastructure.Services;

namespace RCS.Controllers
{
    public class VersionsController : ApiController
    {
        private readonly IVersionService _versionService;

        public VersionsController(IVersionService versionService)
        {
            _versionService = versionService;
        }

        //get versions
        [Route("projects/{projectNum:int}/requirements/{requirementNum:int}/versions")]
        public async Task<IHttpActionResult> GetVersions(int projectNum, int requirementNum)
        {
            var result = await _versionService.GetVersions(projectNum, requirementNum);
            var ordered = result.OrderByDescending(x => x.Id);
            return Ok(ordered);
        }

        //get version
        [Route("projects/{projectNum:int}/requirements/{requirementId}/version")]
        public async Task<IHttpActionResult> GetVersion(int projectNum, string requirementId ="")
        {
            var result = await _versionService.GetVersion(requirementId);
            return Ok(result);
        }

        //get history
        [Route("projects/{projectNum:int}/requirements/{requirementId}/history")]
        public async Task<IHttpActionResult> GetHistory(string requirementId)
        {
            var result = await _versionService.GetHistory(requirementId);
            var ordered = result.OrderBy(x => x.Id);
            return Ok(ordered);
        }

        //update history and version
        [HttpPut]
        [Route("projects/{projectNum:int}/requirements/{requirementId}/history/version")]
        public async Task<IHttpActionResult> UpdateHistory(HistoryDto model)
        {
            var result = await _versionService.UpdateHistoryAndVersion(model);
            return Ok(result);
        }

        //create comment
        [HttpPost]
        [Route("projects/{projectNum:int}/requirements/{requirementId}/comment")]
        public async Task<IHttpActionResult> CreateRequirement(HistoryDto model)
        {
            var result = await _versionService.CreateComment(model);
            return Ok(result);
        }

    }
}

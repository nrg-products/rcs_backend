using System;
using System.Threading.Tasks;
using MongoDB.Driver;
using RCS.Infrastructure.Exeptions;

namespace RCS.Repositories
{
    public class BaseRepository
    {
        public async Task<T> ExecuteFunc<T>(Func<Task<T>> func)
        {
            T result;
            try
            {
                result = await func();
            }
            catch (TimeoutException)
            {
                throw new DataBaseExeption("Can not connect to database");
            }
            catch (Exception e)
            {
                throw new DataBaseExeption(e.Message);
            }

            return result;
        }

        public async Task ExecuteAction(Func<Task> func)
        {
            try
            {
                await func();
            }
            catch (TimeoutException)
            {
                throw new DataBaseExeption("Can not connect to database");
            }
            catch (Exception e)
            {
                throw new DataBaseExeption(e.Message);
            }
        }

        public void CheckExistance<T>(T entity)
        {
            if (entity == null)
            {
                throw new EntityNotFoundException();
            }
        }

        public void CheckExistanceRemove(DeleteResult entity)
        {
            if (entity.DeletedCount != 1)
            {
                throw new EntityNotFoundException();
            }
        }

        public void CheckExistanceUpdate(UpdateResult entity)
        {
            if (entity.ModifiedCount != 1)
            {
                throw new EntityNotFoundException();
            }
        }
    }
}
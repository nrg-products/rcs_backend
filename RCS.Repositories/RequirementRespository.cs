﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.SqlServer.Server;
using MongoDB.Driver;
using RCS.Infrastructure;
using RCS.Infrastructure.Models;
using RCS.Repositories.Entities;

namespace RCS.Repositories
{
    public class RequirementRepository : BaseRepository, IRequirementRepository
    {
        public IMongoCollection<Requirement> Requirements { get; set; }
        public IMongoCollection<Project> Projects { get; set; }
        public IMongoCollection<Counter> Counters { get; set; }

        public RequirementRepository()
            : this("")
        {
        }

        public RequirementRepository(string connection)
        {
            if (string.IsNullOrWhiteSpace(connection))
            {
                connection = "mongodb://oauthAdmin:oauthNrg@oauth-076nl0x1.cloudapp.net:27017/oauth";
            }
            var client = new MongoClient(connection);
            var database = client.GetDatabase("oauth");
            Requirements = database.GetCollection<Requirement>("requirements");
            Projects = database.GetCollection<Project>("projects");
            Counters = database.GetCollection<Counter>("counters");
        }

        public async Task<List<RequirementDto>> GetAllRequirementsAsync(string projectId)
        {
            var filter = Builders<Requirement>.Filter.Eq(x=>x.ProjectId, projectId);
            var result = await ExecuteFunc(() => Requirements.Find(filter).ToListAsync());
            CheckExistance(result);
            return result.Select(item => new RequirementDto
            {
                Name = item.Name,
                Description = item.Description,
                CreatedBy = item.CreatedBy,
                Modified = item.Modified,
                ModifiedBy = item.ModifiedBy,
                Created = item.Created,
                Id = item.Id,
                Num = item.Num,
                ParentId = item.ParentId,
                Type = "requirement",
                DueDate = item.DueDate,
                IssueLink = item.IssueLink,
                Priority = item.Priority,
                Status = item.Status,
                Source = item.Source,
                VersionString = item.VersionString,
                FixedVersionStatus = item.FixedVersionStatus,
                Collapsed = item.Collapsed
            }).ToList();
        }

        public async Task<RequirementDto> GetRequirementAsync(string projectId, int requirementNum)
        {
            var builder = Builders<Requirement>.Filter;
            var filter = builder.Eq(x => x.Num, requirementNum) & builder.Eq(x => x.ProjectId, projectId);
            var result = await ExecuteFunc(() => Requirements.Find(filter).FirstOrDefaultAsync());
            CheckExistance(result);
            return new RequirementDto
            {
                Name = result.Name,
                Description = result.Description,
                CreatedBy = result.CreatedBy,
                Modified = result.Modified,
                ModifiedBy = result.ModifiedBy,
                Created = result.Created,
                Id = result.Id,
                Num = result.Num,
                ParentId = result.ParentId,
                Type = "requirement",
                DueDate = result.DueDate,
                IssueLink = result.IssueLink,
                Priority = result.Priority,
                Status = result.Status,
                Source = result.Source,
                VersionString = result.VersionString,
                FixedVersionStatus = result.FixedVersionStatus,
                Collapsed = result.Collapsed
            };
        }

        public async Task<string> AddRequirementAsync(string projectId, int counterNum, RequirementDto item)
        {
            var requirement = new Requirement
            {
                Num = counterNum,
                ProjectId = projectId,
                Name = item.Name,
                Description = item.Description,
                Source = item.Source,
                Priority = item.Priority,
                FixedVersionStatus = item.FixedVersionStatus,
                ParentId = item.ParentId,
                Created = DateTime.UtcNow,
                CreatedBy = item.CreatedBy,
                Status = item.Status,
                VersionString = "0.1"
            };
            await ExecuteAction(() => Requirements.InsertOneAsync(requirement));
            return requirement.Id;
        }

        public async Task<bool> UpdateRequirementMinor(string projectId, string requirementId, RequirementDto item, string versionString)
        {
            var builder = Builders<Requirement>.Filter;
            var filter = builder.Eq(x => x.Id, requirementId) & builder.Eq(x => x.ProjectId, projectId);
            var update = Builders<Requirement>.Update
                .Set(x => x.Name, item.Name)
                .Set(x => x.Description, item.Description)
                .Set(x => x.Source, item.Source)
                .Set(x => x.ModifiedBy, item.ModifiedBy)
                .Set(x => x.VersionString, versionString)
                .Set(x => x.Modified, DateTime.UtcNow);
            var result = await ExecuteFunc(() => Requirements.UpdateOneAsync(filter, update));
            CheckExistanceUpdate(result);
            return true;
        }

        public async Task<bool> UpdateRequirement(string projectId, string requirementId, RequirementDto item)
        {
            var builder = Builders<Requirement>.Filter;
            var filter = builder.Eq(x => x.Id, requirementId) & builder.Eq(x => x.ProjectId, projectId);
            var update = Builders<Requirement>.Update
                .Set(x => x.IssueLink, item.IssueLink)
                .Set(x => x.Priority, item.Priority)
                .Set(x => x.Status, item.Status)
                .Set(x => x.FixedVersionStatus, item.FixedVersionStatus)
                .Set(x => x.Source, item.Source)
                .Set(x => x.Name, item.Name)
                .Set(x => x.DueDate, item.DueDate);
            var result = await ExecuteFunc(() => Requirements.UpdateOneAsync(filter, update));
            CheckExistanceUpdate(result);
            return true;
        }

        public async Task<bool> UpdateRequirementParent(string projectId, string requirementId, RequirementDto item)
        {
            var builder = Builders<Requirement>.Filter;
            var filter = builder.Eq(x => x.Id, requirementId) & builder.Eq(x => x.ProjectId, projectId);
            var update = Builders<Requirement>.Update
                .Set(x => x.ParentId, item.ParentId)
                .Set(x => x.Collapsed, item.Collapsed);
            var result = await ExecuteFunc(() => Requirements.UpdateOneAsync(filter, update));
            CheckExistanceUpdate(result);
            return true;
        }

        public async Task<bool> UpdateRequirementMajor(string projectId, string requirementId, RequirementDto item, string versionString)
        {
            var builder = Builders<Requirement>.Filter;
            var filter = builder.Eq(x => x.Id, requirementId) & builder.Eq(x => x.ProjectId, projectId);
            var update = Builders<Requirement>.Update
                .Set(x => x.ModifiedBy, item.ModifiedBy)
                .Set(x => x.FixedVersionStatus, item.FixedVersionStatus)
                .Set(x => x.VersionString, versionString)
                .Set(x => x.Modified, DateTime.UtcNow);
            var result = await ExecuteFunc(() => Requirements.UpdateOneAsync(filter, update));
            CheckExistanceUpdate(result);
            return true;
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using RCS.Infrastructure;
using RCS.Infrastructure.Models;
using RCS.Repositories.Entities;

namespace RCS.Repositories
{
    public class ProjectRepository : BaseRepository, IProjectRepository
    {
        public IMongoCollection<Project> Projects { get; set; }

        public ProjectRepository()
            : this("")
        {
        }

        public ProjectRepository(string connection)
        {
            if (string.IsNullOrWhiteSpace(connection))
            {
                connection = "mongodb://oauthAdmin:oauthNrg@oauth-076nl0x1.cloudapp.net:27017/oauth";
            }
            var client = new MongoClient(connection);
            var database = client.GetDatabase("oauth");
            Projects = database.GetCollection<Project>("projects");
        }

        public async Task<List<ProjectDto>> GetAllProjectsAsync()
        {
            var result = await ExecuteFunc(() => Projects.Find(r => true).ToListAsync());
            return result.Select(item => new ProjectDto
            {
                Name = item.Name,
                Id = item.Id,
                Num = item.Num,
                Key = item.Key,
                Members = item.Members
            }).ToList();
        }

        public async Task<ProjectDto> GetProjectAsync(int projectNum)
        {
            var filter = Builders<Project>.Filter.Eq(x => x.Num, projectNum);
            var result = await ExecuteFunc(() => Projects.Find(filter).FirstOrDefaultAsync());
            CheckExistance(result);
            return new ProjectDto
            {
                Name = result.Name,
                Id = result.Id,
                Num = result.Num,
                Key = result.Key,
                Members = result.Members
            };
        }

        public async Task<List<ProjectDto>> GetProjectsByEmailAsync(string email)
        {
            var filter = Builders<Project>.Filter.Eq("Members.Email", email);
            var result = await ExecuteFunc(() => Projects.Find(filter).ToListAsync());
            return result.Select(item => new ProjectDto
            {
                Name = item.Name,
                Id = item.Id,
                Num = item.Num,
                Key = item.Key,
                Members = item.Members
            }).ToList();
        }

        public async Task<bool> RemoveProjectMember(string memberId)
        {
            var filter = Builders<Project>.Filter.Eq("Members.Id", memberId);
            var update = Builders<Project>.Update.PullFilter(x => x.Members,
                Builders<ProjectMember>.Filter.Eq(x => x.Id, memberId));
            await ExecuteFunc(() => Projects.FindOneAndUpdateAsync(filter, update));
            return true;
        }

        public async Task<ProjectDto> AddProjectAsync(ProjectDto item, int counterNum)
        {
            var project = new Project
            {
                Num = counterNum,
                Name = item.Name,
                Key = item.Key,
                Members = item.Members
            };
            await ExecuteAction(() => Projects.InsertOneAsync(project));
            return new ProjectDto
            {
                Id = project.Id,
                Num = project.Num,
                Name = project.Name,
                Key = project.Key,
            };
        }

        public async Task<bool> RemoveProjectAsync(int projectNum)
        {
            var filter = Builders<Project>.Filter.Eq(x => x.Num, projectNum);
            var result = await ExecuteFunc(() => Projects.DeleteOneAsync(filter));
            CheckExistanceRemove(result);
            return true;
        }

        public async Task<bool> UpdateProjectAsync(int projectNum, ProjectDto item)
        {
            var filter = Builders<Project>.Filter.Eq(x => x.Num, projectNum);
            var update = Builders<Project>.Update
                .Set(x => x.Name, item.Name)
                .Set(x => x.Members, item.Members)
                .Set(x => x.Key, item.Key);
            var result = await ExecuteFunc(() => Projects.UpdateOneAsync(filter, update));
            CheckExistanceUpdate(result);
            return true;
        }

        public async Task<bool> UpdateProjectMembersAsync(int projectNum, ProjectDto project)
        {
            var filter = Builders<Project>.Filter.Eq(x => x.Num, projectNum);
            var update = Builders<Project>.Update
                .Set(x => x.Members, project.Members);
            var result = await ExecuteFunc(() => Projects.UpdateOneAsync(filter, update));
            CheckExistanceUpdate(result);
            return true;
        }

        public async Task<bool> AddMemberAsync(int projectNum, ProjectMember member)
        {
            var filter = Builders<Project>.Filter.Eq(x => x.Num, projectNum);
            var project = await ExecuteFunc(() => Projects.Find(filter).FirstOrDefaultAsync());
            if (project.Members == null)
            {
                project.Members = new List<ProjectMember> {member};
            }
            else  project.Members.Add(member);
            var update = Builders<Project>.Update
                .Set(x => x.Members, project.Members);
            var result = await ExecuteFunc(() => Projects.UpdateOneAsync(filter, update));
            CheckExistanceUpdate(result);
            return true;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using RCS.Infrastructure;
using RCS.Infrastructure.Models;
using RCS.Repositories.Entities;
using Version = System.Version;

namespace RCS.Repositories
{
    public class VersionRepository : BaseRepository, IVersionRepository
    {
        public IMongoCollection<Versions> Versions { get; set; }
        public IMongoCollection<History> History { get; set; }
        public IMongoCollection<Project> Projects { get; set; }
        public IMongoCollection<Requirement> Requirements { get; set; }

        public VersionRepository()
            : this("")
        {
        }

        public VersionRepository(string connection)
        {
            if (string.IsNullOrWhiteSpace(connection))
            {
                connection = "mongodb://oauthAdmin:oauthNrg@oauth-076nl0x1.cloudapp.net:27017/oauth";
            }
            var client = new MongoClient(connection);
            var database = client.GetDatabase("oauth");
            Versions = database.GetCollection<Versions>("versions");
            History = database.GetCollection<History>("history");
            Projects = database.GetCollection<Project>("projects");
            Requirements = database.GetCollection<Requirement>("requirements");
        }

        public async Task<List<VersionsDto>> GetAllVersionsAsync(string requirementId)
        {
            var filter = Builders<Versions>.Filter.Eq(x => x.RequirementId, requirementId);
            var result = await ExecuteFunc(() => Versions.Find(filter).ToListAsync());
            CheckExistance(result);
            return result.Select(item => new VersionsDto
            {
                Id = item.Id,
                Created = item.Created,
                CreatedBy = item.CreatedBy,
                VersionString = item.MajorVersion.ToString() + '.' + item.MinorVersion.ToString(),
                MinorVersion = item.MinorVersion,
                MajorVersion = item.MajorVersion,
                ObjectCopy = item.ObjectCopy
            }).ToList();
        }
        public async Task<VersionsDto> GetVersionAsync(string requirementId)
        {
            var filter = Builders<Versions>.Filter.Eq(x => x.RequirementId, requirementId);
            var sort = Builders<Versions>.Sort.Descending(x => x.Id);
            var result = await ExecuteFunc(() => Versions.Find(filter).Sort(sort).FirstOrDefaultAsync());
            CheckExistance(result);
            return new VersionsDto
            {
                Id = result.Id,
                Created = result.Created,
                CreatedBy = result.CreatedBy,
                VersionString = result.MajorVersion.ToString() + '.' + result.MinorVersion,
                MinorVersion = result.MinorVersion,
                MajorVersion = result.MajorVersion,
                ObjectCopy = result.ObjectCopy
            };
        }
        public async Task<List<HistoryDto>> GetAllHistoryAsync(string requirementId)
        {
            var filter = Builders<History>.Filter.Eq(x => x.RequirementId, requirementId);
            var result = await ExecuteFunc(() => History.Find(filter).ToListAsync());
            CheckExistance(result);
            return result.Select(item => new HistoryDto
            {
                Id = item.Id,
                Created = item.Created,
                CreatedBy = item.CreatedBy,
                VersionId = item.VersionId,
                ObjectCopy = item.ObjectCopy,
                UpdatedField = item.UpdatedField,
                Comment = item.Comment,
                StringTemplate = item.StringTemplate
            }).ToList();
        }

        public async Task<VersionsDto> AddVersionAsync(RequirementDto item, string type, int major, int minor)
        {
            var stringTemplate = "Updated";
            if (type == "major")
            {
                major = major + 1;
                minor = 0;
                stringTemplate = "Freezed";
            }
            else if (type == "minor") minor = minor + 1;

            var newVersion = new Versions
            {
                Id = ObjectId.GenerateNewId().ToString(),
                Created = DateTime.UtcNow,
                CreatedBy = item.ModifiedBy,
                RequirementId = item.Id,
                MinorVersion = minor,
                MajorVersion = major,
                ObjectCopy = new ObjectCopyDto
                {
                    Description = item.Description,
                    Name = item.Name,
                    Source = item.Source,
                    DueDate = item.DueDate,
                    FixedVersionStatus = item.FixedVersionStatus,
                    IssueLink = item.IssueLink,
                    VersionString = major.ToString() + '.' + minor
                }
            };
            var newHistory = new History
            {
                RequirementId = item.Id,
                StringTemplate = stringTemplate,
                Created = DateTime.UtcNow,
                CreatedBy = item.ModifiedBy,
                VersionId = newVersion.Id,
                ObjectCopy = new ObjectCopyDto
                {
                    Description = item.Description,
                    Name = item.Name,
                    Source = item.Source,
                    DueDate = item.DueDate,
                    FixedVersionStatus = item.FixedVersionStatus,
                    IssueLink = item.IssueLink,
                    VersionString = major.ToString() + '.' + minor
                }
            };
            await ExecuteAction(() => Versions.InsertOneAsync(newVersion));
            await ExecuteAction(() => History.InsertOneAsync(newHistory));
            return new VersionsDto
            {
                Id = newVersion.Id,
                Created = newVersion.Created,
                CreatedBy = newVersion.CreatedBy,
                VersionString = newVersion.MajorVersion.ToString() + '.' + newVersion.MinorVersion.ToString(),
                MinorVersion = newVersion.MinorVersion,
                MajorVersion = newVersion.MajorVersion,
                ObjectCopy = newVersion.ObjectCopy
            };
        }

        public async Task<VersionsDto> CreateVersionAsync(RequirementDto item)
        {
            var newVersion = new Versions
            {
                Id = ObjectId.GenerateNewId().ToString(),
                Created = DateTime.UtcNow,
                CreatedBy = item.CreatedBy,
                RequirementId = item.Id,
                MinorVersion = 1,
                MajorVersion = 0,
                ObjectCopy = new ObjectCopyDto
                {
                    Description = item.Description,
                    Name = item.Name,
                    Source = item.Source,
                    VersionString = "0.1"
                }
            };
            var newHistory = new History
            {
                RequirementId = item.Id,
                StringTemplate = "Created",
                Created = DateTime.UtcNow,
                CreatedBy = item.CreatedBy,
                VersionId = newVersion.Id,
                ObjectCopy = new ObjectCopyDto
                {
                    Description = item.Description,
                    Name = item.Name,
                    Source = item.Source,
                    VersionString = "0.1"
                }
            };
            await ExecuteAction(() => Versions.InsertOneAsync(newVersion));
            await ExecuteAction(() => History.InsertOneAsync(newHistory));
            return new VersionsDto
            {
                Id = newVersion.Id,
                VersionString = newVersion.MajorVersion.ToString() + '.' + newVersion.MinorVersion,
                MinorVersion = newVersion.MinorVersion,
                MajorVersion = newVersion.MajorVersion,
                ObjectCopy = newVersion.ObjectCopy
            };
        }
        public async Task<bool> CreateHistoryAsync(RequirementDto item)
        {
            var stringTemplate = "Updated";
            var newHistory = new History
            {
                Id = ObjectId.GenerateNewId().ToString(),
                RequirementId = item.Id,
                StringTemplate = stringTemplate,
                UpdatedField = item.UpdatedField,
                Created = DateTime.UtcNow,
                CreatedBy = item.ModifiedBy
            };
            await ExecuteAction(() => History.InsertOneAsync(newHistory));
            return true;
        }
        public async Task<bool> CreateCommentAsync(HistoryDto item)
        {
            var stringTemplate = "Commented";
            var comment = item.Comment;
            var newHistory = new History
            {
                Id = ObjectId.GenerateNewId().ToString(),
                RequirementId = item.Id,
                StringTemplate = stringTemplate,
                Created = DateTime.UtcNow,
                CreatedBy = item.CreatedBy,
                Comment = comment
            };
            await ExecuteAction(() => History.InsertOneAsync(newHistory));
            return true;
        }

        public async Task<bool> UpdateHistoryAsync(HistoryDto item)
        {
            var filter = Builders<History>.Filter.Eq(x => x.Id, item.Id);
            var update = Builders<History>.Update
                .Set(x => x.ObjectCopy, item.ObjectCopy);
            var result = await ExecuteFunc(() => History.UpdateOneAsync(filter, update));
            CheckExistanceUpdate(result);
            return true;
        }

        public async Task<bool> UpdateVersionAsync(HistoryDto item)
        {
            var filter = Builders<Versions>.Filter.Eq(x => x.Id, item.VersionId);
            var update = Builders<Versions>.Update
                .Set(x => x.ObjectCopy, item.ObjectCopy);
            var result = await ExecuteFunc(() => Versions.UpdateOneAsync(filter, update));
            CheckExistanceUpdate(result);
            return true;
        }
    }
}
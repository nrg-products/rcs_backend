﻿using System;
using System.Threading.Tasks;
using MongoDB.Driver;
using RCS.Infrastructure;
using RCS.Infrastructure.Models;
using RCS.Repositories.Entities;

namespace RCS.Repositories
{
    public class CounterRepository : BaseRepository, ICounterRepository
    {
        public IMongoCollection<Counter> Counters { get; set; }

        public CounterRepository()
            : this("")
        {
        }

        public CounterRepository(string connection)
        {
            if (string.IsNullOrWhiteSpace(connection))
            {
                connection = "mongodb://oauthAdmin:oauthNrg@oauth-076nl0x1.cloudapp.net:27017/oauth";
            }
            var client = new MongoClient(connection);
            var database = client.GetDatabase("oauth");
            Counters = database.GetCollection<Counter>("counters");
        }

        public async Task<CounterDto> AddCounter(string key)
        {
            var filter = Builders<Counter>.Filter.Eq(x => x.Key, key);
            var update = Builders<Counter>.Update.Inc(x => x.Num, 1);
            var counter = await ExecuteFunc(() => Counters.FindOneAndUpdateAsync(filter, update));
            if (counter != null)
            {
                return new CounterDto
                {
                    Id = counter.Id,
                    Key = counter.Key,
                    Num = counter.Num
                };
            }
            counter = new Counter
            {
                Key = key,
                Num = 1
            };
            await ExecuteAction(() => Counters.InsertOneAsync(counter));
            counter = await ExecuteFunc(() => Counters.FindOneAndUpdateAsync(filter, update));
            return new CounterDto
            {
                Id = counter.Id,
                Key = counter.Key,
                Num = counter.Num
            };
        }
    }
}

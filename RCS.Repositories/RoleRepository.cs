﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using RCS.Infrastructure;
using RCS.Infrastructure.Models;
using RCS.Repositories.Entities;

namespace RCS.Repositories
{
    public class RoleRepository : BaseRepository, IRoleRepository
    {
        public IMongoCollection<Role> Roles { get; set; }

        public RoleRepository()
            : this("")
        {
        }

        public RoleRepository(string connection)
        {
            if (string.IsNullOrWhiteSpace(connection))
            {
                connection = "mongodb://oauthAdmin:oauthNrg@oauth-076nl0x1.cloudapp.net:27017/oauth";
            }
            var client = new MongoClient(connection);
            var database = client.GetDatabase("oauth");
            Roles = database.GetCollection<Role>("roles");
        }

        public async Task<List<RoleDto>> GetRolesAsync(string projectId)
        {
            var filter = Builders<Role>.Filter.Eq(x => x.ProjectId, projectId);
            var result = await ExecuteFunc(() => Roles.Find(filter).ToListAsync());
            CheckExistance(result);
            return result.Select(item => new RoleDto
            {
                Name = item.Name,
                Id = item.Id,
                ProjectId = item.ProjectId,
                Approval = item.Approval,
                ProjectAccess = item.ProjectAccess
            }).ToList();
        }

        public async Task<RoleDto> AddRoleAsync(RoleDto item)
        {
            var role = new Role
            {
                Name = item.Name,
                Approval = item.Approval,
                ProjectId = item.ProjectId,
                ProjectAccess = item.ProjectAccess
            };
            await ExecuteAction(() => Roles.InsertOneAsync(role));
            return new RoleDto
            {
                Name = role.Name,
                Id = role.Id,
                ProjectId = role.ProjectId,
                Approval = role.Approval,
                ProjectAccess = role.ProjectAccess
            };
        }
    }
}

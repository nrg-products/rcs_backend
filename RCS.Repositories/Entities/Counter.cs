﻿namespace RCS.Repositories.Entities
{
    public class Counter : MongoEntity
    {
        public string Key { get; set; }

        public int Num { get; set; }
    }
}

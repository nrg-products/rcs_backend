﻿namespace RCS.Repositories.Entities
{
    public class Folder : ModifiableEntity
    {
        public int Num { get; set; }

        public string ProjectId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string ParentId { get; set; }

        public string EditDate { get; set; }

        public bool Collapsed { get; set; }
    }
}

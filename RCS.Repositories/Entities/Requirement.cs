﻿using System;

namespace RCS.Repositories.Entities
{
    public class Requirement : ModifiableEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public int Num { get; set; }

        public string ProjectId { get; set; }

        public string Source { get; set; }

        public string IssueLink { get; set; }

        public string Priority { get; set; }

        public string Status { get; set; }

        public DateTime DueDate { get; set; }

        public string ParentId { get; set; }

        public string VersionString { get; set; }

        public string FixedVersionStatus { get; set; }

        public bool Collapsed { get; set; }
    }
}
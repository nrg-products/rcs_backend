﻿using RCS.Infrastructure.Models;

namespace RCS.Repositories.Entities
{
    public class Role : MongoEntity
    {
        public string Name { get; set; }

        public string ProjectId { get; set; }

        public string Approval { get; set; }

        public ProjectAccess ProjectAccess { get; set; }
    }
}

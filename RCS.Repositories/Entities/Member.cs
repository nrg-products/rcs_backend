﻿using RCS.Infrastructure.Models;

namespace RCS.Repositories.Entities
{
    public class Member : ModifiableEntity
    {
        public string ProjectId { get; set; }

        public string InvitationKey { get; set; }

        public string Email { get; set; }

        public RoleDto Role { get; set; }

        public string State { get; set; }

        public string MessageId { get; set; }
    }
}

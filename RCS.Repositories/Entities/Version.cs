﻿using RCS.Infrastructure.Models;

namespace RCS.Repositories.Entities
{
    public class Versions : ModifiableEntity
    {
        public string RequirementId { get; set; }

        public int MinorVersion { get; set; }

        public int MajorVersion { get; set; }

        public ObjectCopyDto ObjectCopy { get; set; }
    }

}

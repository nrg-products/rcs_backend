﻿using RCS.Infrastructure.Models;

namespace RCS.Repositories.Entities
{
    public class History : ModifiableEntity
    {
        public string RequirementId { get; set; }

        public string VersionId { get; set; }

        public ObjectCopyDto ObjectCopy { get; set; }

        public string StringTemplate { get; set; }

        public string Comment { get; set; }

        public string UpdatedField { get; set; }
    }
}

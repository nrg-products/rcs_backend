using System;
using RCS.Infrastructure.Models;

namespace RCS.Repositories.Entities
{
    public class ModifiableEntity : MongoEntity
    {
        public DateTime Created { get; set; }

        public UserDto CreatedBy { get; set; }

        public DateTime Modified { get; set; }

        public UserDto ModifiedBy { get; set; }
    }
}
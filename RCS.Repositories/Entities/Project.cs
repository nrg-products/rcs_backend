﻿using System.Collections.Generic;
using RCS.Infrastructure.Models;

namespace RCS.Repositories.Entities
{
    public class Project : ModifiableEntity
    {
        public string Name { get; set; }
        public int Num { get; set; }
        public string Key { get; set; }
        public List<ProjectMember> Members { get; set; }
    }
}

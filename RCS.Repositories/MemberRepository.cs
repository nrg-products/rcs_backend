﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using RCS.Infrastructure;
using RCS.Infrastructure.Models;
using RCS.Repositories.Entities;

namespace RCS.Repositories
{
    public class MemberRepository : BaseRepository, IMemberRepository
    {
        public IMongoCollection<Project> Projects { get; set; }
        public IMongoCollection<Member> Members { get; set; }
        public MemberRepository()
            : this("")
        {
        }
        
        public MemberRepository(string connection)
        {
            if (string.IsNullOrWhiteSpace(connection))
            {
                connection = "mongodb://oauthAdmin:oauthNrg@oauth-076nl0x1.cloudapp.net:27017/oauth";
            }
            var client = new MongoClient(connection);
            var database = client.GetDatabase("oauth");
            Members = database.GetCollection<Member>("members");
            Projects = database.GetCollection<Project>("projects");
        }

        public async Task<List<MemberDto>> GetAllMembersAsync(string projectId)
        {
            var filter = Builders<Member>.Filter.Eq(x => x.ProjectId, projectId);
            var result = await ExecuteFunc(() => Members.Find(filter).ToListAsync());
            CheckExistance(result);
            return result.Select(item => new MemberDto
            {
                Id = item.Id,
                Email = item.Email,
                Role = item.Role,
                State = item.State,
                MessageId = item.MessageId,
                ProjectId = item.ProjectId
            }).ToList();
        }

        public async Task<MemberDto> AddMemberAsync(MemberDto item, string messageId, string invitationKey, string projectId)
        {
            var state = "Delivered";
            if (messageId == "owner" && invitationKey == "owner") state = "Owner";
            var member = new Member
            {
                ProjectId = projectId,
                Email = item.Email,
                Role = item.Role,
                InvitationKey = invitationKey,
                MessageId = messageId,
                State = state
            };
            await ExecuteAction(() => Members.InsertOneAsync(member));
            return new MemberDto
            {
                Email = member.Email,
                Id = member.Id,
                MessageId = member.MessageId,
                Role = member.Role, 
                State = member.State,
                ProjectId = member.ProjectId
            };
        }

        public async Task<bool> DeleteMemberAsync(string memberId)
        {
            var filter = Builders<Member>.Filter.Eq(x => x.Id, memberId);
            var resultDelete = await ExecuteFunc(() => Members.DeleteOneAsync(filter));
            CheckExistanceRemove(resultDelete);
            return true;
        }

        public async Task<MemberDto> GetMemberAsync(string invitationKey)
        {
            var filter = Builders<Member>.Filter.Eq(x => x.InvitationKey, invitationKey);
            var member = await ExecuteFunc(() => Members.Find(filter).FirstOrDefaultAsync());
            return new MemberDto
            {
                Email = member.Email,
                Role = member.Role,
                State = member.State,
                Id = member.Id,
                ProjectId = member.ProjectId,
                MessageId = member.MessageId
            };
        }

        public async Task<bool> UpdateMemberRoleAsync(string memberId, RoleDto role)
        {
            var builder = Builders<Member>.Filter;
            var filter = builder.Eq(x => x.Id, memberId);
            var update = Builders<Member>.Update
                .Set(x => x.Role, role);
            await ExecuteFunc(() => Members.UpdateOneAsync(filter, update));
            return true;
        }

        public async Task<bool> UpdateMemberStatusAsync(string invitationKey)
        {
            var builder = Builders<Member>.Filter;
            var filter = builder.Eq(x => x.InvitationKey, invitationKey);
            var update = Builders<Member>.Update
                .Set(x => x.State, "Accepted");
            await ExecuteFunc(() => Members.UpdateOneAsync(filter, update));
            return true;
        }
    }
}

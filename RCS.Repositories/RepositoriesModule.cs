﻿using Autofac;
using RCS.Infrastructure;

namespace RCS.Repositories
{
    public class RepositoriesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ProjectRepository>().As<IProjectRepository>();
            builder.RegisterType<FolderRepository>().As<IFolderRepository>();
            builder.RegisterType<RequirementRepository>().As<IRequirementRepository>();
            builder.RegisterType<VersionRepository>().As<IVersionRepository>();
            builder.RegisterType<MemberRepository>().As<IMemberRepository>();
            builder.RegisterType<CounterRepository>().As<ICounterRepository>();
            builder.RegisterType<RoleRepository>().As<IRoleRepository>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using RCS.Infrastructure;
using RCS.Infrastructure.Models;
using RCS.Repositories.Entities;

namespace RCS.Repositories
{
    public class FolderRepository : BaseRepository, IFolderRepository
    {
        public IMongoCollection<Folder> Folders { get; set; }
        public IMongoCollection<Project> Projects { get; set; }
        public IMongoCollection<Counter> Counters { get; set; }
        public FolderRepository()
            : this("")
        {
        }
       
        public FolderRepository(string connection)
        {
            if (string.IsNullOrWhiteSpace(connection))
            {
                connection = "mongodb://oauthAdmin:oauthNrg@oauth-076nl0x1.cloudapp.net:27017/oauth";
            }
            var client = new MongoClient(connection);
            var database = client.GetDatabase("oauth");
            Folders = database.GetCollection<Folder>("folders");
            Projects = database.GetCollection<Project>("projects");
            Counters = database.GetCollection<Counter>("counters");
        }


        public async Task<List<FolderDto>> GetAllFoldersAsync(string projectId)
        {
            var filter = Builders<Folder>.Filter.Eq(x => x.ProjectId, projectId);
            var result = await ExecuteFunc(() => Folders.Find(filter).ToListAsync());
            CheckExistance(result);
            return result.Select(item => new FolderDto
            {
                Name = item.Name, Description = item.Description, Created = item.Created, Modified = item.Modified,
                Id = item.Id, Num = item.Num, ParentId = item.ParentId,
                Type = "folder", Collapsed = item.Collapsed
            }).ToList();
        }

        public async Task<FolderDto> GetFolderAsync(string projectId, int folderNum)
        {
            var builder = Builders<Folder>.Filter;
            var filter = builder.Eq(x=>x.Num, folderNum) & builder.Eq(x=>x.ProjectId, projectId);
            var result = await ExecuteFunc(() => Folders.Find(filter).FirstOrDefaultAsync());
            CheckExistance(result);
            return new FolderDto
            {
                Name = result.Name,
                Description = result.Description,
                Created = result.Created,
                Modified = result.Modified,
                Id = result.Id,
                Num = result.Num,
                ParentId = result.ParentId,
                Type = "folder",
                Collapsed = result.Collapsed
            }; 
        }

        public async Task<bool> AddFolderAsync(string projectId, int counterNum, FolderDto item)
        {
            var folder = new Folder
            {
                Num = counterNum,
                ProjectId = projectId,
                Name = item.Name,
                Description = item.Description,
                ParentId =  item.ParentId,
                Created = DateTime.UtcNow
            };
            await ExecuteAction(() => Folders.InsertOneAsync(folder));
            return true;
        }

        public async Task<bool> RemoveFolderAsync(string projectId, int folderNum)
        {
            var builder = Builders<Folder>.Filter;
            var filter = builder.Eq(x=>x.Num, folderNum) & builder.Eq(x=>x.ProjectId, projectId);
            var result = await ExecuteFunc(() => Folders.DeleteOneAsync(filter));
            CheckExistanceRemove(result);
            return true;
        }

        public async Task<bool> UpdateFolderAsync(string projectId, int folderNum, FolderDto item)
        {
            var builder = Builders<Folder>.Filter;
            var filter = builder.Eq(x=>x.Num, folderNum) & builder.Eq(x=>x.ProjectId, projectId);
            var update = Builders<Folder>.Update
                .Set(x => x.Name, item.Name)
                .Set(x => x.Description, item.Description)
                .Set(x => x.ParentId, item.ParentId)
                .Set(x => x.Collapsed, item.Collapsed)
                .Set(x => x.Modified, DateTime.UtcNow);
            var result = await ExecuteFunc(() => Folders.UpdateOneAsync(filter, update));
            CheckExistanceUpdate(result);
            return true;
        }
    }
}